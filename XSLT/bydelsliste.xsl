<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:output encoding="ISO-8859-1" method="xml" omit-xml-declaration="yes" indent="no"/>

  <xsl:param name="BydelBy"></xsl:param>
  <xsl:param name="BydelNr"></xsl:param>
  <xsl:param name="valgtype">K</xsl:param>

<xsl:variable name="Form">Default.aspx</xsl:variable>

<xsl:template match="respons">
<table width="100%">
  
	<xsl:apply-templates select="/respons/rapport[data[@navn='KommNavn']='Oslo'][1]" mode="oslo"/>
  
</table>
</xsl:template>

<xsl:template match="/respons/rapport" mode="oslo">
	<xsl:variable name="KommNr">0301</xsl:variable>
	<xsl:variable name="KommNavn">Oslo</xsl:variable>
  
  <tr><td>
		<a>
		<xsl:attribute name="href">
			<xsl:value-of select="$Form"/>
      <xsl:text>?BydelBy=</xsl:text>
      <xsl:if test="$KommNr != $BydelBy">
        <xsl:value-of select="$KommNr"/>
      </xsl:if>
      <xsl:text>&amp;VisningIndeks=3</xsl:text>
      <xsl:text>&amp;KommuneNr=</xsl:text>
      <xsl:value-of select="$KommNr"/>

      <xsl:text>&amp;Navn=</xsl:text>
			<xsl:value-of select="$KommNavn"/>
		</xsl:attribute>
		<xsl:value-of select="$KommNavn"/>
		</a>
	<td class="status" align="right">
		<xsl:value-of select="count(../*)"/> bydeler
	</td>
	</td>
	</tr>
  <xsl:if test="$KommNr=$BydelBy">
    <tr>
      <td align="left">
        <a>
          <xsl:attribute name="href">
          <xsl:value-of select="$Form"/>
          <xsl:text>?BydelBy=</xsl:text>
          <xsl:value-of select="$KommNr"/>
          <xsl:text>&amp;VisningIndeks=3</xsl:text>
          <xsl:text>&amp;KommuneNr=</xsl:text>
          <xsl:value-of select="$KommNr"/>
          <xsl:text>&amp;AlleBydeler=1</xsl:text>
          <xsl:text>&amp;Navn=</xsl:text>
          <xsl:value-of select="$KommNavn"/>
        </xsl:attribute>
        - Alle
        </a>
      </td>
      <td></td>

    </tr>
  </xsl:if>
  <xsl:if test="$KommNr=$BydelBy">
    <tr>
		<td colspan="2">
		<table width="100%">
			<xsl:apply-templates select="/respons/rapport[data[@navn='KommNavn'] = $KommNavn and data[@navn='AntFrammotte'] != 0]" mode="bydel">
				<xsl:sort select="data[@navn='BydelNavn']"></xsl:sort>
			</xsl:apply-templates>					
		</table>
		</td>
		</tr>
  </xsl:if>

</xsl:template>

<xsl:template match="*">
</xsl:template>

<xsl:template match="/respons/rapport" mode="bydel">
	<tr>
	<td width="5"></td>
	<td>
		<a>
		<xsl:attribute name="href">
			<xsl:value-of select="$Form"/>
			<xsl:text>?BydelBy=</xsl:text>
      <xsl:value-of select="$BydelBy"/>
      <xsl:text>&amp;VisningIndeks=3</xsl:text>
      <xsl:text>&amp;BydelNr=</xsl:text>
			<xsl:value-of select="data[@navn='BydelNr']"/>
			<xsl:text>&amp;Navn=</xsl:text>
			<xsl:value-of select="data[@navn='BydelNavn']"/>
		</xsl:attribute>
			<xsl:value-of select="data[@navn='BydelNavn']"/>
		</a>
	</td>
	<td class="status" align="right" title="Bydelsoversikt (Antall kretser i bydelen: fhst. opptalt + vtst. opptalt + alt opptalt)">
    <xsl:value-of select="data[@navn='TotAntKretser']"/>
    <xsl:text>:</xsl:text>
		<xsl:value-of select="data[@navn='AntKretserFhstOpptalt']"/>
		<xsl:text>+</xsl:text>
		<xsl:value-of select="data[@navn='AntKretserVtstOpptalt']"/>
		<xsl:text>+</xsl:text>
		<xsl:value-of select="data[@navn='AntKretserAltOpptalt']"/>
	</td>
	</tr>	
</xsl:template>


</xsl:stylesheet>