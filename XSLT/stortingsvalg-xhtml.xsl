<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:output encoding="ISO-8859-1" method="xml" omit-xml-declaration="yes" indent="yes"/>
<xsl:decimal-format name="no" decimal-separator="," grouping-separator=" "/>
<xsl:decimal-format name="pros" decimal-separator="."/>

<xsl:variable name="rapportnavn">
	<xsl:value-of select="/respons/rapport/rapportnavn"/>
</xsl:variable>

<xsl:param name="navn"></xsl:param>
<xsl:param name="Partikategori">
	<xsl:choose>
		<xsl:when test="$rapportnavn='F05' or $rapportnavn='K05' or $rapportnavn='K04'">2</xsl:when>
		<xsl:otherwise>3</xsl:otherwise>
	</xsl:choose>
</xsl:param>
<xsl:param name="status">0</xsl:param>
<xsl:param name="head">yes</xsl:param>

<xsl:variable name="date">
	<xsl:value-of select="/respons/rapport/data[@navn='SisteRegDato']"/>
</xsl:variable>

<xsl:variable name="time">
	<xsl:value-of select="/respons/rapport/data[@navn='SisteRegTid']"/>
</xsl:variable>

<xsl:variable name="dato">
	<xsl:value-of select="substring($date, 7, 2)"/>
	<xsl:text>.</xsl:text>
	<xsl:value-of select="substring($date, 5, 2)"/>
	<xsl:text>.</xsl:text>
	<xsl:value-of select="substring($date, 1, 4)"/>
</xsl:variable>

<xsl:param name="ntbdato">
	<xsl:value-of select="$dato"/>
	<xsl:text> </xsl:text>
	<xsl:value-of select="substring($time, 1, 5)"/>
</xsl:param>

<xsl:param name="normdate">
	<xsl:value-of select="substring($date, 1, 4)"/>
	<xsl:text>.</xsl:text>
	<xsl:value-of select="substring($date, 5, 2)"/>
	<xsl:text>.</xsl:text>
	<xsl:value-of select="substring($date, 7, 2)"/>
	<xsl:text> </xsl:text>
	<xsl:value-of select="$time"/>
</xsl:param>

<xsl:variable name="fylke">
	<xsl:value-of select="/respons/rapport/data[@navn='FylkeNavn']"/>
</xsl:variable>

<xsl:variable name="kommune">
	<xsl:value-of select="/respons/rapport/data[@navn='KommNavn']"/>
</xsl:variable>

<xsl:variable name="krets">
	<xsl:value-of select="/respons/rapport/data[@navn='KretsNavn']"/>
</xsl:variable>

<xsl:variable name="status_text">
	<xsl:choose>
		<xsl:when test="/respons/rapport/data[@navn='StatusInd']='0'">
			<xsl:text>, ingen resultater innsendt</xsl:text> 
		</xsl:when>
		<xsl:when test="/respons/rapport/data[@navn='StatusInd']='1'">
			<xsl:text>, bare foreløpige forhåndsstemmer</xsl:text> 
		</xsl:when>
		<xsl:when test="/respons/rapport/data[@navn='StatusInd']='8'">
			<xsl:text>, korrigert resultat</xsl:text> 
		</xsl:when>
		<xsl:otherwise></xsl:otherwise>
	</xsl:choose>
	<!--
		0.	Ingen resultater innsendt
		1.	Bare foreløpige forhåndsstemmer innsendt
		2.	Bare endelige fhst. innsendt
		3.	Bare foreløpige valgtingsstemmer innsendt
		4.	Bare endelige vtst. innsendt
		5.	Foreløpige fhst. og foreløpige vtst. innsendt
		6.	Endelige fhst. og foreløpige vtst. innsendt
		7.	Foreløpige fhst. og endelige vtst. innsendt
		8.	Endelige fhst. og endelige vtst. innsendt
	-->
</xsl:variable>

<xsl:variable name="prognose">
	<xsl:value-of select=
		"/respons/rapport/tabell/liste[data[@navn='Partikode'] = 'A']/data[@navn='ProgProSt'] != '' and
		(/respons/rapport/data[@navn='TotAntKomm'] - /respons/rapport/data[@navn='AntKommVtstOpptalt'] - /respons/rapport/data[@navn='AntKommAltOpptalt']) != 0"/>
</xsl:variable>
	
<!-- Main template -->
<xsl:template match="respons">
	<xsl:choose>
	<xsl:when test="$navn=''">
		<xsl:choose>
		<xsl:when test="$head='yes'">
			<html>
			<head>
				<xsl:call-template name="title"/>
				<xsl:call-template name="htmlhead"/>
			</head>
				<xsl:call-template name="rapporthode"/>
				<xsl:apply-templates/>
			</html>
		</xsl:when>
		<xsl:otherwise>
			<xsl:call-template name="rapporthode"/>
			<xsl:apply-templates/>
		</xsl:otherwise>
		</xsl:choose>		
	</xsl:when>
	<xsl:otherwise>
		<xsl:call-template name="error"/>
	</xsl:otherwise>
	</xsl:choose>
</xsl:template>

<xsl:template name="error">
	<h2>Resultatet for <xsl:value-of select="$navn"/> er ikke klart ennå!</h2>
</xsl:template>

<xsl:template match="Status">
</xsl:template>

<!-- Template for to tabeller i rapporten  -->
<xsl:template match="rapport">
	
	<table class="liste" border="1"  cellspacing="0"  cellpadding="3">
		<xsl:call-template name="topheader"/>
		<tr><xsl:apply-templates select="tabell/liste[1]/data/@navn"/></tr>
		<xsl:apply-templates select="tabell/liste"/>

		<xsl:choose>
		<xsl:when test="$Partikategori='2'">
			<xsl:call-template name="sumandre">
				<xsl:with-param name="kategori" select="3"/>
			</xsl:call-template>
		</xsl:when>
		</xsl:choose>
		
		<xsl:call-template name="sum"/>
	</table>
	<p/>
	<xsl:choose>
	<xsl:when test="$status=1">
		<table class="statusouter" border="1"  cellspacing="0"  cellpadding="0">
			<tr>
			<td valign="top">
			<table class="status" border="0"  cellspacing="0"  cellpadding="5">
				<xsl:apply-templates select="/respons/rapport/data"/>
			</table>
	<!--
			<table class="status" border="0"  cellspacing="0"  cellpadding="5">
				<xsl:apply-templates select="/respons/rapport/data[(position() div 2) - round(position() div 2) != 0]"/>
			</table>
			</td>
			<td valign="top">
			<table class="status" border="0"  cellspacing="0"  cellpadding="5">
				<xsl:apply-templates select="/respons/rapport/data[(position() div 2) - round(position() div 2) = 0]"/>
			</table>
	-->
			</td>
			</tr>
		</table>
	</xsl:when>
	<xsl:otherwise>
		<xsl:call-template name="status"/>
	</xsl:otherwise>
	</xsl:choose>
</xsl:template>

<xsl:template name="status">
	<!--
		435 av 435 kommuner (435 ferdig opptalt).€
		100,0 prosent av ant. stemmeberettigede.€
		2521781 opptalte stemmer.€
		Frammøte: 39,3 prosent.€
	-->
	<p>
	<xsl:if test="data[@navn='TotAntKomm']">
		<xsl:value-of select="data[@navn='AntKommFhstOpptalt'] + data[@navn='AntKommVtstOpptalt'] + data[@navn='AntKommAltOpptalt']"/>
		<xsl:text> av </xsl:text>
		<xsl:value-of select="data[@navn='TotAntKomm']"/>
		<xsl:text> kommuner (</xsl:text>
		<xsl:value-of select="data[@navn='AntKommAltOpptalt']"/>
		<xsl:text> ferdig opptalt).</xsl:text><br/>
	</xsl:if>

	<xsl:text>Omfatter </xsl:text>
	<xsl:if test="data[@navn='AntStBerett']">
    <xsl:value-of select="format-number((data[@navn='AntFrammotte'] div data[@navn='AntStBerett'] * 100), '0.0', 'pros')"/>
	</xsl:if>	
	<xsl:if test="data[@navn='AntStberett']">
    <xsl:value-of select="format-number((data[@navn='AntFrammotte'] div data[@navn='AntStberett'] * 100), '0.0', 'pros')"/>
	</xsl:if>	
	<xsl:text> prosent av </xsl:text>
	<xsl:choose>
		<xsl:when test="data[@navn='AntStBerett']">
			<xsl:value-of select="format-number(data[@navn='AntStBerett'], '### ### ###', 'no')"/>
		</xsl:when>
		<xsl:otherwise>
			<xsl:value-of select="format-number(data[@navn='AntStberett'], '### ### ###', 'no')"/>
		</xsl:otherwise>
	</xsl:choose>
	<xsl:text> stemmeberettigede.</xsl:text>
	<br/>

	<xsl:value-of select="format-number(data[@navn='AntFrammotte'], '### ### ###', 'no')"/>
	<xsl:text> opptalte stemmer.</xsl:text><br/>

	<xsl:text>Frammøte: </xsl:text>
	<xsl:value-of select="data[@navn='ProFrammotte']"/>
	<xsl:text> prosent.</xsl:text><br/>
	</p>
</xsl:template>

<!-- Template for sum i tabellen -->
<xsl:template name="sum">
	<tr>
	<td><i><b>Sum</b></i></td>
	<td align="right"><i><b>
    <nobr>
	<!--<xsl:value-of select="format-number(sum(//liste[data/@navn='Partikode' and data[@navn='Partikode'] != 'Andre']/data[@navn='AntStemmer']), '### ##0', 'no')"/>-->
	<xsl:value-of select="format-number(/respons/rapport/data[@navn='AntFrammotte'], '### ##0', 'no')"/>
      </nobr>
    </b></i></td>
	<td align="right">&#160;</td>
	<td align="right">&#160;</td>
	<td align="right">&#160;</td>
	<xsl:if test="/respons/rapport/rapportnavn = 'F05' or /respons/rapport/rapportnavn = 'F04'">
		<td align="right">&#160;</td>
		<td align="right">&#160;</td>
		<td align="right">&#160;</td>
		<td align="right">&#160;</td>
		<td align="right">&#160;</td>
		<td align="right">&#160;</td>
		<td align="right">&#160;</td>
	</xsl:if>
	<xsl:if test="/respons/rapport/rapportnavn = 'ST06' or $rapportnavn='ST04'">
    <td style="width:1 px;background-color:#cccccc;"></td>
    <td align="right">&#160;</td>
		<td align="right">&#160;</td>
		<td align="right">&#160;</td>
		<td align="right">&#160;</td>
		<td align="right">&#160;</td>
    <td align="right">&#160;</td>
		<td align="right">&#160;</td>
		<td align="right">&#160;</td>
		<td align="right">&#160;</td>
		<td align="right">&#160;</td>
		<td align="right">&#160;</td>
	</xsl:if>
	<xsl:if test="/respons/rapport/rapportnavn = 'K02' or /respons/rapport/rapportnavn = 'K09'">
		<td align="right">&#160;</td>
		<td align="right">&#160;</td>
		<td align="right">&#160;</td>
		<td align="right">&#160;</td>
		<td align="right">&#160;</td>
		<td align="right">&#160;</td>
		<td align="right">&#160;</td>
	</xsl:if>
	<xsl:if test="/respons/rapport/rapportnavn = 'K05' or /respons/rapport/rapportnavn = 'K04'">
		<td align="right">&#160;</td>
	</xsl:if>
	<xsl:if test="/respons/rapport/rapportnavn = 'F02'">
		<td align="right">&#160;</td>
		<td align="right">&#160;</td>
	</xsl:if>
	<xsl:if test="/respons/rapport/rapportnavn = 'ST02'">
		<td align="right">&#160;</td>
		<td align="right">&#160;</td>
		<td align="right">&#160;</td>
	</xsl:if>
<!--
	<xsl:if test="/respons/rapport/rapportnavn = 'ST04'">
		<td align="right">&#160;</td>
		<td align="right">&#160;</td>
		<td align="right">&#160;</td>
		<td align="right">&#160;</td>
		<td align="right">&#160;</td>
	</xsl:if>
-->
	<xsl:if test="/respons/rapport/rapportnavn = 'ST03'">
		<td align="right">&#160;</td>
		<td align="right">&#160;</td>
	</xsl:if>
	</tr>
</xsl:template>

<!-- Template for topheader i tabellen -->
<xsl:template name="topheader">
	<tr>
	<xsl:choose>
		<xsl:when test="/respons/rapport/rapportnavn = 'F05' or /respons/rapport/rapportnavn = 'F04'">
			<th colspan="5">Valgnatt resultater</th>
			<th colspan="7">Prognoser</th>
		</xsl:when>
		<xsl:when test="/respons/rapport/rapportnavn = 'F03' or /respons/rapport/rapportnavn = 'K03'">
			<th colspan="5">Valgnatt resultater</th>
		</xsl:when>
		<xsl:when test="/respons/rapport/rapportnavn = 'F02'">
			<th colspan="7">Valgnatt resultater</th>
		</xsl:when>
		<xsl:when test="/respons/rapport/rapportnavn = 'K05' or /respons/rapport/rapportnavn = 'K04' ">
			<th colspan="6">Valgnatt resultater</th>
		</xsl:when>
		<xsl:when test="/respons/rapport/rapportnavn = 'K02'">
			<th colspan="7">Valgnatt resultater</th>
			<th colspan="5">Prognoser</th>
		</xsl:when>
		<xsl:when test="/respons/rapport/rapportnavn = 'K09'">
			<th colspan="6">Valgnatt resultater</th>
			<th colspan="6">Prognoser</th>
		</xsl:when>
		<xsl:when test="/respons/rapport/rapportnavn = 'ST06'">
			<th colspan="5" align="left">Valgnatt resultater</th>
      <th style="width:1 px;background-color:#cccccc;"></th>
      <th colspan="12" align="left">Prognoser</th>
		</xsl:when>
		<xsl:when test="$rapportnavn = 'ST04'">
			<th colspan="5" align="left">Valgnatt resultater</th>
      <th style="width:1 px;background-color:#cccccc;"></th>
      <th colspan="12" align="left">Prognoser</th>
		</xsl:when>
		<xsl:when test="$rapportnavn = 'ST02'">
			<th colspan="12">Valgnatt resultater</th>
		</xsl:when>
		<xsl:when test="$rapportnavn = 'ST03'">
			<th colspan="12">Valgnatt resultater</th>
		</xsl:when>
    <xsl:when test="$rapportnavn = 'ST05'">
      <th colspan="12">Valgnatt resultater</th>
    </xsl:when>
  </xsl:choose>
	</tr>
</xsl:template>

<!-- Template for sum i tabellen -->
<xsl:template name="sumandre">
	<xsl:param name="kategori"/>
	<tr>
	<td>Andre (lister)</td>
<!--
	<td>Andre (<xsl:value-of select="$kategori"/>)</td>
	<td align="right"><xsl:value-of select="format-number(sum(//liste[data/@navn='Partikategori' and data[@navn='Partikategori'] = $kategori]/data[@navn='AntFhst']), '### ##0', 'no')"/></td>
-->
	<td align="right"><xsl:value-of select="format-number(sum(//liste[data/@navn='Partikategori' and data[@navn='Partikategori'] = $kategori]/data[@navn='AntStemmer']), '### ##0', 'no')"/></td>
	<td align="right">&#160;</td>
	<td align="right">&#160;</td>
	<td align="right">&#160;</td>
	<xsl:if test="/respons/rapport/rapportnavn = 'F05' or /respons/rapport/rapportnavn = 'F04'">
		<td align="right">&#160;</td>
		<td align="right">&#160;</td>
		<td align="right">&#160;</td>
		<td align="right">&#160;</td>
		<td align="right">&#160;</td>
		<td align="right">&#160;</td>
		<td align="right">&#160;</td>
	</xsl:if>
	<xsl:if test="/respons/rapport/rapportnavn = 'ST06'">
    <td style="width:1 px;background-color:#cccccc;"></td>
		<td align="right">&#160;</td>
		<td align="right">&#160;</td>
		<td align="right">&#160;</td>
		<td align="right">&#160;</td>
		<td align="right">&#160;</td>
    <td align="right">&#160;</td>
		<td align="right">&#160;</td>
		<td align="right">&#160;</td>
		<td align="right">&#160;</td>
		<td align="right">&#160;</td>
		<td align="right">&#160;</td>
	</xsl:if>
	<xsl:if test="/respons/rapport/rapportnavn = 'K02' or /respons/rapport/rapportnavn = 'K09'">
		<td align="right">&#160;</td>
		<td align="right">&#160;</td>
		<td align="right">&#160;</td>
		<td align="right">&#160;</td>
		<td align="right">&#160;</td>
		<td align="right">&#160;</td>
		<td align="right">&#160;</td>
	</xsl:if>
	<xsl:if test="/respons/rapport/rapportnavn = 'K05' or /respons/rapport/rapportnavn = 'K04'">
		<td align="right">&#160;</td>
	</xsl:if>
	<xsl:if test="/respons/rapport/rapportnavn = 'F02'">
		<td align="right">&#160;</td>
		<td align="right">&#160;</td>
	</xsl:if>
	<xsl:if test="/respons/rapport/rapportnavn = 'ST02'">
		<td align="right">&#160;</td>
		<td align="right">&#160;</td>
		<td align="right">&#160;</td>
	</xsl:if>
	<xsl:if test="/respons/rapport/rapportnavn = 'ST04'">
		<td align="right">&#160;</td>
		<td align="right">&#160;</td>
		<td align="right">&#160;</td>
		<td align="right">&#160;</td>
		<td align="right">&#160;</td>
	</xsl:if>
	</tr>
</xsl:template>

<!-- Template for status-rader i rapporten  -->
<xsl:template match="/respons/rapport/data">
	<tr>
		<xsl:attribute name="class">
		<xsl:value-of select="@navn"/>
		</xsl:attribute>
	<td class="ledetekst">
	<xsl:call-template name="ledetekst">
		<xsl:with-param name="navn">
			<xsl:value-of select="@navn"/>
		</xsl:with-param>
	</xsl:call-template>
	</td>
	<td class="colon">:</td>
	<td align="right">
	<xsl:attribute name="class">
		<xsl:value-of select="@navn"/>
	</xsl:attribute>
	<xsl:value-of select="."/>
	</td>
	</tr>
</xsl:template>


<!-- Template for ubrukte rader i tabellen 
<xsl:template match="liste">
</xsl:template>
-->

<!-- Template for rader i tabellen -->
<xsl:template match="liste">
	<xsl:if test="data[@navn='Partikategori'] &lt;= $Partikategori and (data[@navn='Partikategori'] != 0 or $Partikategori = 1)">
	<tr><xsl:apply-templates/></tr>
	</xsl:if>
</xsl:template>

<!-- Template for ubrukte kolonner i tabell header-->
<xsl:template match="liste/data/@navn">
</xsl:template>

<!-- Template for kolonner i tabell header-->
<!--<xsl:template match="liste/data[@navn!='Partikategori' and @navn!='AntFhst' and @navn!='DiffProgPropFFtv' and @navn!='DiffProgPropFStv' and @navn!='DiffProgPropFKsv']/@navn">-->
<xsl:template match="liste/data[@navn!='Partikategori' and @navn!='AntFhst']/@navn">
	<xsl:variable name="navn"><xsl:value-of select="."/></xsl:variable>
<!--	<th>-->
	<xsl:choose>
		<xsl:when test="$navn='Partikode'">
			<th><xsl:text>Parti</xsl:text></th>
		</xsl:when>
		<xsl:when test="$navn='AntStemmer'">
			<th><xsl:text>Stemmer</xsl:text></th>
		</xsl:when>
		<xsl:when test="$navn='ProSt'">
			<th><xsl:text>%</xsl:text></th>
		</xsl:when>
		<xsl:when test="$navn='DiffPropFStv'">
			<th><xsl:text>09-05 %</xsl:text></th>
		</xsl:when>
		<xsl:when test="$navn='DiffPropFFtv'">
			<th><xsl:text>09-07 %</xsl:text></th>
		</xsl:when>
		<xsl:when test="$navn='DiffPropFKsv'">
			<th><xsl:text>09-07 %</xsl:text></th>
		</xsl:when>
		<xsl:when test="$navn='DiffStFKsv'">
			<th><xsl:text>Diff. 07</xsl:text></th>
		</xsl:when>
		<xsl:when test="$navn='DiffStFStv'">
			<th><xsl:text>Diff. 05</xsl:text></th>
		</xsl:when>
		<xsl:when test="$navn='DiffStFFtv'">
			<th><xsl:text>Diff. 07</xsl:text></th>
		</xsl:when>
		<xsl:when test="$navn='DiffPropFhstFStv'">
			<th><xsl:text>Diff. Fhst</xsl:text></th>
		</xsl:when>

	<!-- Prognoser -->

    <xsl:when test="$navn='ProgProSt'">
      <td style="width:1 px;background-color:#cccccc;"></td>
      <th><xsl:text>%</xsl:text></th>
		</xsl:when>

		<xsl:when test="$navn='ProgAntMndtFtv'">
			<th><xsl:text>Mandater</xsl:text></th>
		</xsl:when>

		<xsl:when test="$navn='ProgAntMndtStv'">
			<th><xsl:text>Mandater</xsl:text></th>
		</xsl:when>

		<xsl:when test="$navn='DiffProgAntMndtFFtv' or $navn='DiffProgAntMndtFtv'">
			<th><xsl:text>Endring mandater</xsl:text></th>
		</xsl:when>

		<xsl:when test="$navn='DiffProgAntMndtStv' or $navn='DiffProgAntMndtFStv'">
			<th><xsl:text>Endring Mandater</xsl:text></th>
		</xsl:when>

		<xsl:when test="$navn='ProgAntMndtStv'">
			<th><xsl:text>Mand. tenkt Stortingsv.</xsl:text></th>
		</xsl:when>
		
		<xsl:when test="$navn='ProgAntUtjMndtStv' or $navn='ProgUtjMndtStv'">
			<th><xsl:text>Utj. mandater (Prognose)</xsl:text></th>
		</xsl:when>

		<xsl:when test="$navn='ProgVinneMisteUtjInd' or $navn='ProgVinneMisteStvInd'">
			<th><xsl:text>+/-</xsl:text></th>
		</xsl:when>
		
		<xsl:when test="$navn='ProgAntStMisteUtj' or $navn='ProgAntStMiste'">
			<th><xsl:text>Mistes</xsl:text></th>
		</xsl:when>

		<xsl:when test="$navn='ProgPropAntStMisteUtj' or $navn='ProgPropAntStMiste'">
			<th><xsl:text>Mistes (%)</xsl:text></th>
		</xsl:when>
		
		<xsl:when test="$navn='ProgAntStVinneUtj' or $navn='ProgAntStVinne'">
			<th><xsl:text>Ant.Vinn</xsl:text></th>
		</xsl:when>

		<xsl:when test="$navn='ProgPropAntStVinneUtj' or $navn='ProgPropAntStVinne'">
			<th><xsl:text>Ant.Vinn (%)</xsl:text></th>
		</xsl:when>
		
		<xsl:when test="$navn='DiffAntMndt'">
			<th><xsl:text>Endring mandater</xsl:text></th>
		</xsl:when>
		<xsl:when test="$navn='ProgAntMndt'">
			<th><xsl:text>Mandater</xsl:text></th>
		</xsl:when>
		<xsl:when test="$navn='DiffProgAntMndt'">
			<th><xsl:text>Endring mandater</xsl:text></th>
		</xsl:when>

		<xsl:when test="$navn='DiffProgPropFFtv'">
			<th><xsl:text>09-07 %</xsl:text></th>
		</xsl:when>

		<xsl:when test="$navn='DiffProgPropFStv'">
			<th><xsl:text>09-05 %</xsl:text></th>
		</xsl:when>

		<xsl:when test="$navn='DiffProgPropFKsv'">
			<th><xsl:text>09-07 %</xsl:text></th>
		</xsl:when>
<!--		<xsl:otherwise>
			<xsl:value-of select="."/>
		</xsl:otherwise>-->
	</xsl:choose>
<!--	</th>-->
</xsl:template>

<!-- Template for ubrukte kolonner i tabellen -->
<xsl:template match="liste/data">
</xsl:template>

<!-- Template for kolonner i tabellen -->
<!--<xsl:template match="liste/data[@navn!='Partikategori' and @navn!='AntFhst' and @navn!='DiffProgPropFFtv' and @navn!='DiffProgPropFStv' and @navn!='DiffProgPropFKsv']">
-->
<xsl:template match="liste/data[@navn!='Partikategori' and @navn!='AntFhst']">
	<xsl:variable name="navn"><xsl:value-of select="@navn"/></xsl:variable>
<!--	<td>
	<xsl:attribute name="class">
		<xsl:value-of select="@navn"/>
	</xsl:attribute>-->

  <xsl:if test="position() = 7 and ($rapportnavn = 'ST04' or $rapportnavn = 'ST06')">
    <td style="width:1 px;background-color:#cccccc;"></td>
  </xsl:if>
  <xsl:choose>
<!--
		<xsl:when test="$navn='ProgVinneMisteStvInd'">
		</xsl:when>
		<xsl:when test="$navn='ProgAntStMiste'">
		</xsl:when>
		<xsl:when test="$navn='ProgPropAntStMiste'">
		</xsl:when>
		<xsl:when test="$navn='ProgAntStVinne'">
		</xsl:when>
		<xsl:when test="$navn='ProgPropAntStVinne'">
		</xsl:when>
		<xsl:when test="$navn='ProgUtjMndtStv'">
		</xsl:when> -->
		<xsl:when test="$navn='ProgSisteMndtRang'">
		</xsl:when>
		
		<xsl:when test="(position() = 3 and $rapportnavn != 'ST05') or (position() = 2 and $rapportnavn = 'ST05')">
		<td>
			<xsl:attribute name="align">right</xsl:attribute>
			<xsl:if test=". != ''">
				<xsl:value-of select="format-number(., '### ##0', 'no')"/>
			</xsl:if>
			<xsl:if test=".=''">&#160;</xsl:if>
		</td>
		</xsl:when>
     <xsl:when test="position() &gt; 1">
		<td>
			<xsl:attribute name="align">right</xsl:attribute>
			<xsl:value-of select="."/>
			<xsl:if test=".=''">&#160;</xsl:if>
		</td>			
		</xsl:when>
    <xsl:otherwise>
		<td>
			<xsl:value-of select="."/>
			<xsl:if test=".=''">&#160;</xsl:if>
		</td>
		</xsl:otherwise>

	</xsl:choose>
	
<!--	<xsl:if test=".=''">&#160;</xsl:if>
	</td>-->
</xsl:template>

<xsl:template name="rapporthode">
	<xsl:variable name="rapportnavn">
		<xsl:value-of select="rapport/rapportnavn"/>
	</xsl:variable>
	
	<h1>
	<xsl:value-of select="$rapportnavn"/>
	<xsl:text>: </xsl:text>

	<xsl:choose>
		<xsl:when test="$rapportnavn = 'F01'">
			<xsl:text>Opptalte kommuner - fylkestingsvalg</xsl:text>
		</xsl:when>
		<xsl:when test="$rapportnavn = 'F02'">
			<xsl:text>Enkeltresultat pr. kommune - fylkestingsvalg</xsl:text>
		</xsl:when>
		<xsl:when test="$rapportnavn = 'F03'">
			<xsl:text>Enkeltoversikt pr. krets - fylkestingsvalg</xsl:text>
		</xsl:when>
		<xsl:when test="$rapportnavn = 'F04'">
			<xsl:text>Fylkesoversikt - fylkestingsvalg</xsl:text>
		</xsl:when>
		<xsl:when test="$rapportnavn = 'F05'">
			<xsl:text>Landsoversikt pr. parti - fylkestingsvalg</xsl:text>
		</xsl:when>
		<xsl:when test="$rapportnavn = 'F07'">
			<xsl:text>Landsoversikt pr. fylke - fylkestingsvalg</xsl:text>
		</xsl:when>
		
		<xsl:when test="$rapportnavn = 'K01'">
			<xsl:text>Opptalte kommuner - kommunevalg</xsl:text>
		</xsl:when>
		<xsl:when test="$rapportnavn = 'K02'">
			<xsl:text>Enkeltresultat pr. kommune - kommunevalg</xsl:text>
		</xsl:when>
		<xsl:when test="$rapportnavn = 'K03'">
			<xsl:text>Enkeltoversikt pr. krets - kommunevalg</xsl:text>
		</xsl:when>
		<xsl:when test="$rapportnavn = 'K04'">
			<xsl:text>Fylkesoversikt - kommunevalg</xsl:text>
		</xsl:when>
		<xsl:when test="$rapportnavn = 'K05'">
			<xsl:text>Landsoversikt pr. parti - kommunevalg</xsl:text>
		</xsl:when>
		<xsl:when test="$rapportnavn = 'K07'">
			<xsl:text>Frammøte – 10 høyeste og 10 laveste - kommunevalg</xsl:text>
		</xsl:when>
		<xsl:when test="$rapportnavn = 'K08'">
			<xsl:text>Bydelsresultater i Oslo - kommunevalg</xsl:text>
		</xsl:when>
		<xsl:when test="$rapportnavn = 'K09'">
			<xsl:text>Bystyreoversikt i Oslo - kommunevalg</xsl:text>
		</xsl:when>
		<xsl:when test="$rapportnavn = 'ST02'">
			<xsl:text>Enkeltresultat pr. kommune - stortingsvalg</xsl:text>
		</xsl:when>
		<xsl:when test="$rapportnavn = 'ST03'">
			<xsl:text>Kretsoversikt - </xsl:text>
			<xsl:value-of select="$krets"/>
		</xsl:when>
		<xsl:when test="$rapportnavn = 'ST04'">
			<xsl:text>Fylkesoversikt - </xsl:text>
			<xsl:value-of select="rapport/data[@navn='FylkeNavn']"/>
		</xsl:when>
		<xsl:when test="$rapportnavn = 'ST06'">
			<xsl:text>Landsoversikt pr. parti - stortingsvalg</xsl:text>
		</xsl:when>
    <xsl:when test="$rapportnavn = 'ST05'">
      <xsl:text>Bydelsoversikt - stortingsvalg</xsl:text>
    </xsl:when>
  </xsl:choose>
	</h1>

	<!-- Skriv Fylkesnavn, Kommunenavn og Kretsnavn -->
	<xsl:choose>
		<xsl:when test="$rapportnavn='K02' or $rapportnavn='F02' or $rapportnavn='ST02'">
			<h2>
			<xsl:value-of select="rapport/data[@navn='KommNavn']"/>
			<xsl:text> i </xsl:text>
			<xsl:value-of select="rapport/data[@navn='FylkeNavn']"/>
			<xsl:value-of select="$status_text"/>
			</h2>
		</xsl:when>
		<xsl:when test="$rapportnavn='K04' or $rapportnavn='F04'">
			<h2>
			<xsl:value-of select="rapport/data[@navn='FylkeNavn']"/>
			</h2>
		</xsl:when>
		<xsl:when test="$rapportnavn='K03' or $rapportnavn='F03'">
			<h2>
			<xsl:value-of select="rapport/data[@navn='KretsNavn']"/>
			<xsl:text> i </xsl:text>
			<xsl:value-of select="rapport/data[@navn='KommNavn']"/>
			<xsl:value-of select="$status_text"/>
			</h2>
    </xsl:when>
    <xsl:when test="$rapportnavn='ST05'">
      <h2>
        <xsl:value-of select="rapport/data[@navn='BydelNavn']"/>
        <xsl:value-of select="$status_text"/>
      </h2>
    </xsl:when>
  </xsl:choose>
	
	<p class="regdato">
	<b>Siste registrering:</b>
	<xsl:text> </xsl:text> 
	<b><xsl:value-of select="$dato"/></b>
	<xsl:text> </xsl:text> 
	<b><xsl:value-of select="substring(rapport/data[@navn='SisteRegTid'], 1, 5)"/></b>


		<i>
		<xsl:choose>
			<xsl:when test="$rapportnavn = 'ST02' and ($kommune = 'Bergen' or $kommune = 'Stavanger' or $kommune = 'Trondheim' or $kommune = 'Oslo')">
				<xsl:text> (Status: </xsl:text>
				<xsl:value-of select="rapport/data[@navn='ProgProStOpptalt']"/>
				<xsl:text> % av stemmene opptalt)</xsl:text>
			</xsl:when>
			<xsl:when test="$rapportnavn = 'ST04' or $rapportnavn = 'ST06'">
				<xsl:text> (Status: </xsl:text>
				<xsl:value-of select="rapport/data[@navn='ProgProStOpptalt']"/>
				<xsl:text> % av stemmene opptalt)</xsl:text>
			</xsl:when>
      <xsl:when test="$rapportnavn = 'ST05'">
      </xsl:when>
      <xsl:otherwise>
				<xsl:text> (Status: </xsl:text>
				<xsl:value-of select="rapport/data[@navn='StatusInd']"/>
				<xsl:text>)</xsl:text>
			</xsl:otherwise>
		</xsl:choose>

		</i> 

	</p>

</xsl:template>

<!-- Template for endring av ledetekster -->
<xsl:template name="ledetekst">
	<xsl:param name="navn"/>
	<xsl:choose>
		<xsl:when test="$navn='SisteRegDato'">
			<xsl:text>Registert dato</xsl:text>
		</xsl:when>
		<xsl:when test="$navn='SisteRegTid'">
			<xsl:text>Registert tid</xsl:text>
		</xsl:when>
    <xsl:when test="$navn='StatusInd'">
      <xsl:text>Indikator for opptellingsstatus</xsl:text>
    </xsl:when>
    <xsl:when test="$navn='FylkeNr'">
			<xsl:text>Fylkenummer</xsl:text>
		</xsl:when>
		<xsl:when test="$navn='FylkeNavn'">
			<xsl:text>Fylkenavn</xsl:text>
		</xsl:when>
		<xsl:when test="$navn='KretsNr'">
			<xsl:text>Kretsummer</xsl:text>
		</xsl:when>
		<xsl:when test="$navn='KretsNavn'">
			<xsl:text>Kretsnavn</xsl:text>
		</xsl:when>
		<xsl:when test="$navn='KommNr'">
			<xsl:text>Kommunenummer</xsl:text>
		</xsl:when>
		<xsl:when test="$navn='KommNavn'">
			<xsl:text>Kommunenavn</xsl:text>
		</xsl:when>

		<xsl:when test="$navn='DiffFrammFFtv'">
			<xsl:text>Endring i antall frammøtte i forhold til forrige fylkestingsvalg</xsl:text>
		</xsl:when>
		<xsl:when test="$navn='DiffPropFrammFFtv'">
			<xsl:text>Endring i prosentpoeng for antall frammøtte i forhold til forrige fylkestingsvalg</xsl:text>
		</xsl:when>

		<xsl:when test="$navn='DiffFrammFKsv'">
			<xsl:text>Endring i ant. frammøtte i forhold til forrige kommunestyrevalg</xsl:text>
		</xsl:when>
		<xsl:when test="$navn='DiffPropFrammFKsv'">
			<xsl:text>Endring i prosentpoeng for ant. frammøtte i forhold til forrige kommunestyrevalg</xsl:text>
		</xsl:when>
		<xsl:when test="$navn='DiffFrammFStv'">
			<xsl:text>Endring i ant. frammøtte i forhold til forrige stortingsvalg</xsl:text>
		</xsl:when>
		<xsl:when test="$navn='DiffPropFrammFStv'">
			<xsl:text>Endring i prosentpoeng for ant. frammøtte i forhold til forrige stortingsvalg</xsl:text>
		</xsl:when>

		<xsl:when test="$navn='TotAntKomm'">
			<xsl:text>Totalt antall kommuner</xsl:text>
		</xsl:when>
		<xsl:when test="$navn='AntKommFhstOpptalt'">
			<xsl:text>Antall kommuner hvor bare forhåndsstemmer er opptalt.</xsl:text>
		</xsl:when>
		<xsl:when test="$navn='AntKommVtstOpptalt'">
			<xsl:text>Antall kommuner hvor bare valgtingsstemmer er opptalt</xsl:text>
		</xsl:when>
		<xsl:when test="$navn='AntKommAltOpptalt'">
			<xsl:text>Antall kommuner hvor både forhånds- og valgtingsstemmer er opptalt</xsl:text>
		</xsl:when>
    <xsl:when test="$navn='TotAntKretser'">
			<xsl:text>Totalt antall kretser i fylket</xsl:text>
		</xsl:when>
		<xsl:when test="$navn='AntKretserFhstOpptalt'">
			<xsl:text>Antall kretser hvor bare forhåndsstemmer er opptalt</xsl:text>
		</xsl:when>
		<xsl:when test="$navn='AntKretserVtstOpptalt'">
			<xsl:text>Antall kretser hvor bare valgtingsstemmer er opptalt</xsl:text>
		</xsl:when>
		<xsl:when test="$navn='AntKretserAltOpptalt'">
			<xsl:text>Antall kretser hvor både forhånds- og valgtingsstemmer er opptalt</xsl:text>
		</xsl:when>

    <xsl:when test="$navn='AntStBerett' or $navn='AntStberett'">
			<xsl:text>Antall stemmeberettigede</xsl:text>
		</xsl:when>
		<xsl:when test="$navn='AntFrammotte'">
			<xsl:text>Antall frammøtte</xsl:text>
		</xsl:when>
		<xsl:when test="$navn='ProFrammotte'">
			<xsl:text>Frammøteprosent</xsl:text>
		</xsl:when>
		<xsl:when test="$navn='AntFhstOpptalt'">
			<xsl:text>Antall forhåndsstemmer</xsl:text>
		</xsl:when>
	
		<!-- Added for Valg2005 -->		
		
		<xsl:when test="$navn='AntRepKvinner'">
			<xsl:text>Antall kvinnelige representanter </xsl:text>
		</xsl:when>
		<xsl:when test="$navn='AntRepMenn'">
			<xsl:text>Antall mannlige representanter </xsl:text>
		</xsl:when>
		<xsl:when test="$navn='AntRepUkjKjonn'">
			<xsl:text>Antall representanter hvor kjønnet er ukjent</xsl:text>
		</xsl:when>

    <!-- Added for Valg2009 -->
    <xsl:when test="$navn='StDevFylke'">
      <xsl:text>Standardavvik for fylket</xsl:text>
    </xsl:when>
    <xsl:when test="$navn='ProgProStOpptalt'">
      <xsl:text>Prognose for prosentandel av stemmene som er opptalt til nå</xsl:text>
    </xsl:when>
    <xsl:when test="$navn='AntKommAltOppt8'">
      <xsl:text>Antall kommuner hvor alle stemmer er opptalt og kontrollert</xsl:text>
    </xsl:when>
    <xsl:when test="$navn='AntKretserAltOppt8'">
      <xsl:text>Antall kretser hvor alle stemmer er opptalt og kontrollert</xsl:text>
    </xsl:when>
    <xsl:when test="$navn='PartikodeMiste'">
      <xsl:text>Partikode for partiet som er nærmest å miste sistemandatet i fylket</xsl:text>
    </xsl:when>
    <xsl:when test="$navn='PartikodeVinne'">
      <xsl:text>Partikode for partiet som er nærmest å vinne sistemandatet i fylket</xsl:text>
    </xsl:when>
    <xsl:when test="$navn='AntStMiste'">
      <xsl:text>Antall stemmer partiet kan miste uten å miste sistemandatet</xsl:text>
    </xsl:when>
    <xsl:when test="$navn='AntStVinne'">
      <xsl:text>Antall stemmer partiet må ha for å vinne sistemandatet</xsl:text>
    </xsl:when>


    <xsl:otherwise>
			<xsl:value-of select="$navn"/>
		</xsl:otherwise>
	</xsl:choose>
</xsl:template>

<xsl:template name="htmlhead">
	<link href="Styles.css" type="text/css" rel="stylesheet" /> 
	<meta name="ntb-date">
		<xsl:attribute name="content">
			<xsl:value-of select="$ntbdato"/>
		</xsl:attribute>
	</meta>
	<meta name="status-ind">
		<xsl:attribute name="content"><xsl:value-of select="/respons/rapport/data[@navn='StatusInd']"/></xsl:attribute>
	</meta>
	<meta name="sistereg">
		<xsl:attribute name="content"><xsl:value-of select="$normdate"/></xsl:attribute>
	</meta>
	<meta name="rapportnavn">
		<xsl:attribute name="content"><xsl:value-of select="/respons/rapport/rapportnavn"/></xsl:attribute>
	</meta>
	<meta name="fylkenr">
		<xsl:attribute name="content"><xsl:value-of select="/respons/rapport/data[@navn='FylkeNr']"/></xsl:attribute>
	</meta>
	<meta name="fylkenavn">
		<xsl:attribute name="content"><xsl:value-of select="$fylke"/></xsl:attribute>
	</meta>
	<meta name="kommunenr">
		<xsl:attribute name="content"><xsl:value-of select="/respons/rapport/data[@navn='KommNr']"/></xsl:attribute>
	</meta>
	<meta name="kommunenavn">
		<xsl:attribute name="content"><xsl:value-of select="$kommune"/></xsl:attribute>
	</meta>
	<meta name="kretsnr">
		<xsl:attribute name="content"><xsl:value-of select="/respons/rapport/data[@navn='KretsNr']"/></xsl:attribute>
	</meta>
	<meta name="kretsnavn">
		<xsl:attribute name="content"><xsl:value-of select="$krets"/></xsl:attribute>
	</meta>
</xsl:template>

<xsl:template name="title">
<title>
	<xsl:choose>
		<xsl:when test="$rapportnavn = 'F01' or $rapportnavn = 'K01'">
			<xsl:text>Opptalte kommuner</xsl:text>
		</xsl:when>
		<xsl:when test="$rapportnavn = 'F02' or $rapportnavn = 'K02'">
			<xsl:text>Enkeltresultat </xsl:text>
			<xsl:value-of select="$kommune"/>
			<xsl:text> kommune</xsl:text>
			<!-- i <xsl:value-of select="$fylke"/>-->
		</xsl:when>
		<xsl:when test="$rapportnavn = 'F03' or $rapportnavn = 'K03'">
			<xsl:text>Kretsoversikt </xsl:text>
			<xsl:value-of select="$krets"/> i <xsl:value-of select="$kommune"/>
		</xsl:when>
		<xsl:when test="$rapportnavn = 'F04' or $rapportnavn = 'K04'">
			<xsl:text>Fylkesoversikt </xsl:text>
			<xsl:value-of select="$fylke"/>
		</xsl:when>
		<xsl:when test="$rapportnavn = 'F05' or $rapportnavn = 'K05'">
			<xsl:text>Landsoversikt pr. parti</xsl:text>
		</xsl:when>
		<xsl:when test="$rapportnavn = 'F07' or $rapportnavn = 'K07'">
			<xsl:text>Landsoversikt pr. fylke</xsl:text>
		</xsl:when>

		<xsl:when test="$rapportnavn = 'K08'">
			<xsl:text>Bydelsresultater i Oslo</xsl:text>
		</xsl:when>
		<xsl:when test="$rapportnavn = 'K09'">
			<xsl:text>Bystyreoversikt</xsl:text>
		</xsl:when>
    <xsl:when test="$rapportnavn = 'ST05'">
      <xsl:text>Bydelsoversikt</xsl:text>
    </xsl:when>
  </xsl:choose>

	<xsl:choose>
		<xsl:when test="substring-before($rapportnavn, '0') = 'F'">
			<xsl:text> - fylkestingsvalg</xsl:text>
		</xsl:when>
    <xsl:when test="substring-before($rapportnavn, '0') = 'K'">
      <xsl:text> - kommunevalg</xsl:text>
    </xsl:when>
    <xsl:when test="substring-before($rapportnavn, '0') = 'ST'">
      <xsl:text> - stortingsvalg</xsl:text>
    </xsl:when>
    <xsl:otherwise>
			<xsl:text> - UKJENT</xsl:text>
		</xsl:otherwise>
	</xsl:choose>
</title>
</xsl:template>


</xsl:stylesheet>